import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class EventsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            events: [
                {id:'xyz1', date: "2021-03-28", name: "QWEE",},
                {id:'xyz2', date: "2021-03-29", name: "ASDF"}
            ],
            start: '2021-03-21',
            end: '2021-04-05' 
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(event) {
        /*const response = await api.events({ date: '' });*/
        /*this.setState({ events: response });*/
        this.setState({ start: "2021-04-01" });
    }

    //event  -- event-controller.js
    //events -- 
    render() {
        return <div>
            <button onClick={this.handleReload} style={{backgroundColor: '#B8E3AB'}}>Reload</button>   
            <h3>Event table</h3>
            <h4>Upcoming events from {this.state.start} to  {this.state.end}</h4>
            <table>
                {this.state.events.map(
                    (event) =>
                            <tr><td>id: {event.id}</td><td>name: {event.name} date: {event.date}</td></tr>)}
            </table>
        </div>
    }
}

export default EventsTable;