import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class CurrentEvents extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            events: [],
            targetDate: '2020-03-29' };

        this.handleReload = this.handleReload.bind(this);
    }


    async handleReload(event) {
        const response = await api.events({ date: this.state.targetDate });
        this.setState({ events:/* [{id:"1", name:"V"}, {id:"2", name:'aaa'}]*/  response});  //было: events: response
        //this.setState({ start: "2021-04-01" });
    }


    render() {
        return <div>
            <button onClick={this.handleReload}>Reload_Current</button>
            
            <table>
                {this.state.events.map(
                    (event) =>
                            <tr><td>id: {event.id}</td><td>name: {event.name} date: {event.date}</td></tr>)}
            </table>
        </div>
    }
}

export default CurrentEvents;