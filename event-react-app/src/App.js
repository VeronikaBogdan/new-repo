import logo from './logo.svg';
import './App.css';
import React from 'react';
import EventsTable from './components/EventsTable';
import CurrentEvents from './components/CurrentEvents';
import Places from './components/Places';
import Bands from './components/Bands';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useLocation
} from 'react-router-dom';

function App() {
  return (

    <Router>
    <div className="container">
        <nav>
          <ul className="Nav">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/places">Places_ET</Link>
            </li>
            <li>
              <Link to="/bands">Bands_C</Link>
            </li>
            <li>
              <Link to="/comments">Comments</Link>
            </li>
          </ul>
        </nav>

        {/* */}

      <div className="App">
        <section>
          <Switch>
            <Route path="/bands">
              <Bands />
            </Route>
            <Route path="/comments">
              <Comments />
            </Route>
            <Route path="/places/:id">
              <Places />
            </Route>
            <Route path="/">
              <h3>This is our main page. </h3>
              <br></br>
              <p style={{fontSize: ' 25px', color: '#1F8873'}}>Welcome to check <Link to={'/places'}>concerts</Link> or reviews <Link to={'/bands'}>now</Link></p>
            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>
          <Route path="/places">
              <Redirect to="/places/2021-04-20" />
          </Route>
        </section>      
      </div> 
    </div>
    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

function Comments() {
  return <div>
         
            <h2 style={{fontSize: ' 30px', color: '#071D70'}}>Comments of our users and attenders<br></br></h2>
            <div style={{fontSize: ' 20px', backgroundColor: '#FCB0BC', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Lorem ipsum dolor sit amet consectetur, adipiscing elit ligula pharetra, 
                potenti sagittis eget inceptos. Pretium ante rhoncus sapien nunc nostra dictumst ac magna quis sollicitudin risus,
                </p>
            </div>
            <div style={{fontSize: ' 20px', backgroundColor: '#FCB0BC', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Nostra enim tellus dictumst porta justo habitasse convallis pellentesque ullamcorper, 
                parturient posuere gravida felis mi varius rhoncus viverra, imperdiet mus id porttitor tortor vulputate facilisis iaculis.
                </p>
            </div>
            <div style={{fontSize: ' 20px', backgroundColor: '#FCB0BC', textAlign: 'left', padding: ' 20px', marginTop: '20px'}}>
                <p>
                Lorem ipsum dolor sit amet consectetur, adipiscing elit ligula pharetra, 
                potenti sagittis eget inceptos. 
                </p>
            </div>
        </div>
}

export default App;
